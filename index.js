//Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

var mongoose = require('mongoose');

const path = require("path");

//Import router
const drinkRouter = require("./app/routes/drinkRouter.js");
const voucherRouter = require("./app/routes/voucherRouter.js");
const userRouter = require("./app/routes/userRouter.js");
const orderRouter = require("./app/routes/orderRouter.js");

//Khởi tạo app express
const app = express();

const port = 8000;

//Khai báo middleware đọc json
app.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

app.use(express.static(__dirname + '/views'));

//Khai báo API dạng get "/" sẽ chạy vào đây
//Callback funtion
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/pizza365index.html'));
})


mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", (error) => {
    if (error) { 
        throw error 
    };
    console.log('Connect MongooDB successfully!');
})


app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
})
app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})

//Khai báo API dạng get "/" sẽ chạy vào đây
//Callback funtion
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.use("/", drinkRouter);
app.use("/", voucherRouter);
app.use("/", userRouter);
app.use("/", orderRouter);

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng): " + port)
})