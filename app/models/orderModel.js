//B1: Import mongooseJS
const mongoose = require('mongoose');

//B2:  Khai baos Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//B3: Khởi tạo 1 schema với các thuộc tính được yêu cầu
const reviewSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    orderId: {
        type: String,
        unique: true
    },
    kichCo: {
        type: String,
        required: true
    },
    suon: {
        type: String,
        required: true
    },
    salad: {
        type: String,
        required: true
    },
    duongKinh: {
        type: String,
        required: true
    },
    hoTen: {
        type: String,
        required: true
    },
    thanhTien: {
        type: String,
        required: true
    },
    giamGia: {
        type: String,
        required: false
    },
    email: {
        type: String,
        required: true
    },
    soDienThoai: {
        type: String,
        required: true
    },
    diaChi: {
        type: String,
        required: true
    },
    loiNhan: {
        type: String,
        required: false
    },
    loaiPizza: {
        type: String,
        required: true
    },
    idVourcher: {
        type: String,
        ref: "Voucher"
    },
    idLoaiNuocUong: {
        type: String,
        ref: "Drink"
    },
    trangThai: {
        type: String,
        required: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }

})

//B4: Export ra 1 model nhờ schema vừa khai báo
module.exports = mongoose.model("Order", reviewSchema)
