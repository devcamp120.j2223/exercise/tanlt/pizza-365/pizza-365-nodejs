// Import drink model vao controller
const drinkModel = require("../models/drinkModel");

// Import mongooseJS
const mongoose = require("mongoose");

const createDrink = (request, response) => {
    //B1: Thu thập dữ liệu
    var bodyRequest = request.body;
    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.maNuocUong) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "maNuocUong is required"
        })
    }
    if (!bodyRequest.tenNuocUong) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "tenNuocUong is required"
        })
    }
    if (!(bodyRequest.donGia && bodyRequest.donGia > 0)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "donGia is required"
        })
    }
    //Thao tác với cơ sở dữ liệu
    let createDrink = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: bodyRequest.maNuocUong,
        tenNuocUong: bodyRequest.tenNuocUong,
        donGia: bodyRequest.donGia
    }

    drinkModel.create(createDrink, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: drink Created",
                data: data
            })
        }
    })

}

const getAllDrink = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    drinkModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(202).json({
                status: "Success: Get drinks sucess",
                data: data
            })
        }
    })
}

const getDrinkById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let drinkId = request.params.drinkId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(drinkId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "drink ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    drinkModel.findById(drinkId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get drinks sucess",
                data: data
            })
        }
    })
}

const updateDrinkById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let drinkId = request.params.drinkId;
    var bodyRequest = request.body;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(drinkId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "drink ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    let drinkUpdate = {
        maNuocUong: bodyRequest.maNuocUong,
        tenNuocUong: bodyRequest.tenNuocUong,
        donGia: bodyRequest.donGia
    }
    drinkModel.findByIdAndUpdate(drinkId, drinkUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: Update drink success",
                data: data
            })
        }
    })
}

const deleteDrinkById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let drinkId = request.params.drinkId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(drinkId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "drink ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    drinkModel.findByIdAndDelete(drinkId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(204).json({
                status: "Success: Delete drinks sucess",
            })
        }
    })
}
const getAllDrinkPizza = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    drinkModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(202).json({
               drinks: data
            })
        }
    })
}
module.exports = {
    createDrink: createDrink,
    getAllDrink: getAllDrink,
    getDrinkById: getDrinkById,
    updateDrinkById: updateDrinkById,
    deleteDrinkById: deleteDrinkById,
    getAllDrinkPizza: getAllDrinkPizza
}