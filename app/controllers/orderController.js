// Import course model vao controller
const userModel = require("../models/userModel");
const orderModel = require("../models/orderModel");
const drinkModel = require("../models/drinkModel");
const voucherModel = require("../models/voucherModel");

// Import mongooseJS
const mongoose = require("mongoose");

const createOrderOfUser = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let userId = request.params.userId;
    let requestBody = request.body;

    console.log(requestBody);
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    if (!requestBody.orderId) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "orderId is required"
        })
    }
    if (!requestBody.pizzaSize) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "pizzaSize is required"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    //drinkModel.findById(requestBody.drink, (error, data) => { console.log(data.tenNuocUong) })
    let orderInput = {
        _id: mongoose.Types.ObjectId(),
        orderId: requestBody.orderId,
        kichCo: requestBody.kichCo,
        duongKinh: requestBody.duongKinh,
        suon: requestBody.suon,
        salad: requestBody.salad,
        loaiPizza: requestBody.loaiPizza,
        idVourcher: requestBody.idVourcher,
        idLoaiNuocUong: requestBody.idLoaiNuocUong,
        soLuongNuoc: requestBody.soLuongNuoc,
        hoTen: requestBody.hoTen,
        thanhTien: requestBody.thanhTien,
        email: requestBody.email,
        soDienThoai: requestBody.soDienThoai,
        diaChi: requestBody.diaChi,
        loiNhan: requestBody.loiNhan,
        trangThai: requestBody.trangThai
    }
    orderModel.create(orderInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            userModel.findByIdAndUpdate(userId,
                {
                    $push: { orders: data._id },
                },
                (error, updateCourse) => {
                    if (error) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        })
                    }
                    else {
                        return response.status(201).json({
                            status: "Create Order Of User",
                            data: data
                        })
                    }
                }
            )
        }
    })

}

const getAllOrderOfUser = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    userModel.findById(userId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                return response.status(200).json({
                    status: "Get data success",
                    data: data.orders
                })
            }
        })
}

const getOrderById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(orderId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: "Success: Get order sucess",
                data: data
            })
        }
    })
}

const updateOrderById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    var requestBody = request.body;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(orderId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    let orderUpdate = {
        kichCo: requestBody.kichCo,
        duongKinh: requestBody.duongKinh,
        suon: requestBody.suon,
        salad: requestBody.salad,
        loaiPizza: requestBody.loaiPizza,
        idVourcher: requestBody.idVourcher,
        idLoaiNuocUong: requestBody.idLoaiNuocUong,
        soLuongNuoc: requestBody.soLuongNuoc,
        hoTen: requestBody.hoTen,
        thanhTien: requestBody.thanhTien,
        email: requestBody.email,
        soDienThoai: requestBody.soDienThoai,
        diaChi: requestBody.diaChi,
        loiNhan: requestBody.loiNhan,
        trangThai: requestBody.trangThai
    }
    orderModel.findByIdAndUpdate(orderId, orderUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: "Success: Update order success",
                data: data
            })
        }
    })
}

const deleteOrderById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let userId = request.params.userId;
    let orderId = request.params.orderId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }
    if (!(mongoose.Types.ObjectId.isValid(orderId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    orderModel.findByIdAndDelete(orderId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            //Sau khi xóa xong 1 review khỏi collection cần xóa thêm orderId trong course đang chứa nó
            userModel.findByIdAndUpdate(userId,
                {
                    $pull: { reviews: orderId }
                },
                (errore) => {
                    if (error) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        })
                    }
                    else {
                        return response.status(204).json({
                            status: "Success: Delete review success",
                        })
                    }
                }
            )
        }
    })
}

const createOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu

    var vObjectRequest = {
        kichCo: request.body.kichCo,
        duongKinh: request.body.duongKinh,
        suon: request.body.suon,
        salad: request.body.salad,
        loaiPizza: request.body.loaiPizza,
        idVourcher: request.body.idVourcher,
        idLoaiNuocUong: request.body.idLoaiNuocUong,
        soLuongNuoc: request.body.soLuongNuoc,
        hoTen: request.body.hoTen,
        thanhTien: request.body.thanhTien,
        email: request.body.email,
        soDienThoai: request.body.soDienThoai,
        diaChi: request.body.diaChi,
        loiNhan: request.body.loiNhan,
        trangThai: "open"
    }
    // Random 1 giá trị xúc xắc bất kỳ
    var randomOrderCode = (Math.random() + 1).toString(36).substring(2)
    // B2: Validate dữ liệu từ request body
    if (!vObjectRequest.kichCo) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "kichCo is required"
        })
    }
    if (!vObjectRequest.duongKinh) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "duongKinh is required"
        })
    }
    if (!vObjectRequest.suon) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "suon is required"
        })
    }
    if (!vObjectRequest.idVourcher) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "idVourcher is required"
        })
    }
    if (!vObjectRequest.idLoaiNuocUong) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "idLoaiNuocUong is required"
        })
    }
    if (!vObjectRequest.soLuongNuoc) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "soLuongNuoc is required"
        })
    }
    if (!vObjectRequest.hoTen) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "hoTen is required"
        })
    }
    if (!vObjectRequest.thanhTien) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "thanhTien is required"
        })
    }
    if (!vObjectRequest.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    if (!vObjectRequest.soDienThoai) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "soDienThoai is required"
        })
    }
    if (!vObjectRequest.diaChi) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "diaChi is required"
        })
    }

    //   Sử dụng userModel tìm kiếm bằng username
    userModel.findOne({
        email: vObjectRequest.email
    }, (errorFindUser, userExist) => {
        if (errorFindUser) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindUser.message
            })
        } else {
            if (!userExist) {
                // Nếu user không tồn tại trong hệ thống
                // Tạo user mới
                userModel.create({
                    _id: mongoose.Types.ObjectId(),
                    fullName: vObjectRequest.hoTen,
                    email: vObjectRequest.email,
                    address: vObjectRequest.diaChi,
                    phone: vObjectRequest.soDienThoai
                }, (errCreateUser, userCreated) => {
                    if (errCreateUser) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: errCreateUser.message
                        })
                    } else {
                        orderModel.create({
                            _id: mongoose.Types.ObjectId(),
                            orderId: randomOrderCode,
                            kichCo: vObjectRequest.kichCo,
                            duongKinh: vObjectRequest.duongKinh,
                            suon: vObjectRequest.suon,
                            salad: vObjectRequest.salad,
                            loaiPizza: vObjectRequest.loaiPizza,
                            idVourcher: vObjectRequest.idVourcher,
                            idLoaiNuocUong: vObjectRequest.idLoaiNuocUong,
                            soLuongNuoc: vObjectRequest.soLuongNuoc,
                            hoTen: vObjectRequest.hoTen,
                            thanhTien: vObjectRequest.thanhTien,
                            email: vObjectRequest.email,
                            soDienThoai: vObjectRequest.soDienThoai,
                            diaChi: vObjectRequest.diaChi,
                            loiNhan: vObjectRequest.loiNhan,
                            trangThai: vObjectRequest.trangThai
                        }, (error, data) => {
                            if (error) {
                                return response.status(500).json({
                                    status: "Error 500: Internal server error",
                                    message: error.message
                                })
                            }
                            else {
                                userModel.findByIdAndUpdate(userCreated._id,
                                    {
                                        $push: { orders: data._id },
                                    },
                                    (error, updateCourse) => {
                                        if (error) {
                                            return response.status(500).json({
                                                status: "Error 500: Internal server error",
                                                message: error.message
                                            })
                                        }
                                        else {
                                            return response.status(201).json({
                                                status: "Create Order Of User",
                                                data: data
                                            })
                                        }
                                    }
                                )
                            }
                        })
                    }
                })
            } else {
                var userId = userExist._id
                orderModel.create({
                    _id: mongoose.Types.ObjectId(),
                    orderId: randomOrderCode,
                    kichCo: vObjectRequest.kichCo,
                    duongKinh: vObjectRequest.duongKinh,
                    suon: vObjectRequest.suon,
                    salad: vObjectRequest.salad,
                    loaiPizza: vObjectRequest.loaiPizza,
                    idVourcher: vObjectRequest.idVourcher,
                    idLoaiNuocUong: vObjectRequest.idLoaiNuocUong,
                    soLuongNuoc: vObjectRequest.soLuongNuoc,
                    hoTen: vObjectRequest.hoTen,
                    thanhTien: vObjectRequest.thanhTien,
                    email: vObjectRequest.email,
                    soDienThoai: vObjectRequest.soDienThoai,
                    diaChi: vObjectRequest.diaChi,
                    loiNhan: vObjectRequest.loiNhan,
                    trangThai: vObjectRequest.trangThai
                }, (error, data) => {
                    if (error) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        })
                    }
                    else {
                        userModel.findByIdAndUpdate(userId,
                            {
                                $push: { orders: data._id },
                            },
                            (error, updateCourse) => {
                                if (error) {
                                    return response.status(500).json({
                                        status: "Error 500: Internal server error",
                                        message: error.message
                                    })
                                }
                                else {
                                    return response.status(201).json({
                                        status: "Create Order Of User",
                                        data: data
                                    })
                                }
                            }
                        )
                    }
                })
            }
        }
    })
}

const getAllOrderPizza = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    orderModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(202).json({
               orders: data
            })
        }
    })
}
module.exports = {
    createOrderOfUser: createOrderOfUser,
    getAllOrderOfUser: getAllOrderOfUser,
    getOrderById: getOrderById,
    updateOrderById: updateOrderById,
    deleteOrderById: deleteOrderById,
    createOrder: createOrder,
    getAllOrderPizza: getAllOrderPizza

}