// Import voucher model vao controller
const voucherModel = require("../models/voucherModel");

// Import mongooseJS
const mongoose = require("mongoose");

const createVoucher = (request, response) => {
    //B1: Thu thập dữ liệu
    var bodyRequest = request.body;
    console.log(bodyRequest);
    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.maVoucher) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "maVoucher is required"
        })
    }
    if (!bodyRequest.phanTramGiamGia) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "phanTramGiamGia is required"
        })
    }
    //Thao tác với cơ sở dữ liệu
    let createVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: bodyRequest.maVoucher,
        phanTramGiamGia: bodyRequest.phanTramGiamGia,
        ghiChu: bodyRequest.ghiChu
    }

    voucherModel.create(createVoucher, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: voucher Created",
                data: data
            })
        }
    })

}

const getAllVoucher = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(202).json({
                status: "Success: Get vouchers sucess",
                data: data
            })
        }
    })
}

const getVoucherById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "voucher ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get vouchers sucess",
                data: data
            })
        }
    })
}
const getVoucherByCode = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let maVoucher = request.params.maVoucher;
    //B2 Validate dữ liệu
    let condition = {};
    
    if (maVoucher) {
        condition.maVoucher = maVoucher;
    }
    console.log(condition)
    //B3 Thao tác với cơ sỡ dữ liệu
    voucherModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                data: data[0]
            })
        }
    })
}

const updateVoucherById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId;
    var bodyRequest = request.body;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "voucher ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    let voucherUpdate = {
        maVoucher: bodyRequest.maVoucher,
        phanTramGiamGia: bodyRequest.phanTramGiamGia,
        ghiChu: bodyRequest.ghiChu
    }
    voucherModel.findByIdAndUpdate(voucherId, voucherUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: Update voucher success",
                data: data
            })
        }
    })
}

const deleteVoucherById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "voucher ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(204).json({
                status: "Success: Delete vouchers sucess",
            })
        }
    })
}

module.exports = {
    createVoucher: createVoucher,
    getAllVoucher: getAllVoucher,
    getVoucherById: getVoucherById,
    updateVoucherById: updateVoucherById,
    deleteVoucherById: deleteVoucherById,
    getVoucherByCode:getVoucherByCode
}