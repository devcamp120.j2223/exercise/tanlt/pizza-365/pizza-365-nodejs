// Import course model vao controller
const userModel = require("../models/userModel");

// Import mongooseJS
const mongoose = require("mongoose");

const createUser = (request, response) => {
    //B1: Thu thập dữ liệu
    var bodyRequest = request.body;
    console.log(bodyRequest);
    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.fullName) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "fullName is required"
        })
    }
    if (!bodyRequest.email) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "email is required"
        })
    }
    if (!bodyRequest.address) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "address is required"
        })
    }
    if (!bodyRequest.phone) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "phone is required"
        })
    }
    //Thao tác với cơ sở dữ liệu
    let createUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: bodyRequest.fullName,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
    }

    userModel.create(createUser, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: User Created",
                data: data
            })
        }
    })

}

const getAllUser = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    userModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(202).json({
                status: "Success: Get users sucess",
                data: data
            })
        }
    })
}

const getUserById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    userModel.findById(userId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get users sucess",
                data: data
            })
        }
    })
}

const updateUserById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let userId = request.params.userId;
    var bodyRequest = request.body;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    let userUpdate = {
        fullName: bodyRequest.fullName,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
    }
    userModel.findByIdAndUpdate(userId, userUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: Update user success",
                data: data
            })
        }
    })
}

const deleteUserById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            response.status(204).json({
                status: "Success: Delete users sucess",
            })
        }
    })
}

const limitUser = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let limitUser = request.query.limit
    //B2 Validate dữ liệu
    if (limitUser) {
        userModel.find().limit(limitUser).exec((error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                response.status(202).json({
                    status: "Success: Get users limit sucess",
                    data: data
                })
            }
        })
    }
    else {
        //B3 Thao tác với cơ sỡ dữ liệu
        userModel.find((error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                response.status(202).json({
                    status: "Success: Get users sucess",
                    data: data
                })
            }
        })
    }
}

const skipUser = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let skipUser = request.query.skip
    //B2 Validate dữ liệu
    if (skipUser) {
        userModel.find().skip(skipUser).exec((error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                response.status(202).json({
                    status: "Success: Get users limit sucess",
                    data: data
                })
            }
        })
    }
    else {
        //B3 Thao tác với cơ sỡ dữ liệu
        userModel.find((error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                response.status(202).json({
                    status: "Success: Get users sucess",
                    data: data
                })
            }
        })
    }
}

const sortUser = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
        userModel.find().sort({ fullName: 'asc'}).exec((error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                response.status(202).json({
                    status: "Success: Get users limit sucess",
                    data: data
                })
            }
        })
}

const skipLimitUser = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let limitUser = request.query.limit
    let skipUser = request.query.skip
    //B2 Validate dữ liệu
    if (skipUser || limitUser) {
        userModel.find().skip(skipUser).limit(limitUser).exec((error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                response.status(202).json({
                    status: "Success: Get users limit sucess",
                    data: data
                })
            }
        })
    }
    else {
        //B3 Thao tác với cơ sỡ dữ liệu
        userModel.find((error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                response.status(202).json({
                    status: "Success: Get users sucess",
                    data: data
                })
            }
        })
    }
}

const findAllUser = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let limitUser = request.query.limit
    let skipUser = request.query.skip
    //B2 Validate dữ liệu
    if (skipUser || limitUser) {
        userModel.find().skip(skipUser).limit(limitUser).sort({ fullName: 'asc'}).exec((error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                response.status(202).json({
                    status: "Success: Get users limit sucess",
                    data: data
                })
            }
        })
    }
    else {
        //B3 Thao tác với cơ sỡ dữ liệu
        userModel.find((error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else {
                response.status(202).json({
                    status: "Success: Get users sucess",
                    data: data
                })
            }
        })
    }
}
module.exports = {
    createUser: createUser,
    getAllUser: getAllUser,
    getUserById: getUserById,
    updateUserById: updateUserById,
    deleteUserById: deleteUserById,
    limitUser: limitUser,
    skipUser: skipUser,
    sortUser: sortUser,
    skipLimitUser: skipLimitUser,
    findAllUser: findAllUser
}