
const express = require("express");

const router = express.Router();

const {createOrderOfUser, getAllOrderOfUser, getOrderById, updateOrderById, deleteOrderById, createOrder, getAllOrderPizza}= require("../controllers/orderController")


router.post("/users/:userId/orders", createOrderOfUser)

router.get("/users/:userId/orders", getAllOrderOfUser)

router.get("/orders/:orderId", getOrderById)

router.put("/orders/:orderId", updateOrderById)

router.delete("/users/:userId/orders/:orderId", deleteOrderById)

router.post("/devcamp-pizza365/orders", createOrder)

router.get("/devcamp-pizza365/orders", getAllOrderPizza)

//Export dữ liệu thành 1 module
module.exports = router;