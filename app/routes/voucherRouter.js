
const express = require("express");

const router = express.Router();
const { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById, getVoucherByCode } = require("../controllers/voucherController")

router.get("/vouchers", getAllVoucher);

router.post("/vouchers", createVoucher);

router.get("/vouchers/:voucherId", getVoucherById)

router.put("/vouchers/:voucherId", updateVoucherById)

router.delete("/vouchers/:voucherId", deleteVoucherById)

router.get("/devcamp-pizza365/vouchers/:maVoucher", getVoucherByCode)
//Export dữ liệu thành 1 module
module.exports = router;