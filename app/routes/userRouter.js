
const express = require("express");

const router = express.Router();
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById, limitUser, skipUser, sortUser, skipLimitUser, findAllUser } = require("../controllers/userController")

router.get("/users", getAllUser);

router.post("/users", createUser);

router.get("/users/:userId", getUserById)

router.put("/users/:userId", updateUserById)

router.delete("/users/:userId", deleteUserById)

router.get("/limit-users", limitUser)

router.get("/skip-users", skipUser)

router.get("/sort-users", sortUser)

router.get("/skip-limit-users", skipLimitUser)

router.get("/sort-skip-limit-users", findAllUser)

//Export dữ liệu thành 1 module
module.exports = router;