
const express = require("express");

const router = express.Router();
const { createDrink, getAllDrink, getDrinkById, updateDrinkById, deleteDrinkById, getAllDrinkPizza } = require("../controllers/drinkController")

router.get("/drinks", getAllDrink);

router.post("/drinks", createDrink);

router.get("/drinks/:drinkId", getDrinkById)

router.put("/drinks/:drinkId", updateDrinkById)

router.delete("/drinks/:drinkId", deleteDrinkById)

router.get("/devcamp-pizza365/drinks", getAllDrinkPizza)

//Export dữ liệu thành 1 module
module.exports = router;