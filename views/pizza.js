$(document).ready(function () {
  onPageLoading();

  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  // bạn có thể dùng để lưu trữ combo được chọn, 
  // mỗi khi khách chọn menu S, M, L bạn lại đổi giá trị properties của nó
  var gSelectedMenuStructure = {
    menuName: "...",    // S, M, L
    duongKinhCM: 0,
    suonNuong: 0,
    saladGr: 0,
    drink: 0,
    loai: "",
    priceVND: 0
  }
  var gInfo = {
    hoten: "",
    email: "",
    dienthoai: "",
    diachi: "",
    loinhan: "",
    magiamgia: ""
  }
  var gOrderId = 0;
  // bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
  var gSelectedPizzaType = "";
  // danh sách các mã giảm giá

  //gọi mẫu lấy danh sách order
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  $("#btn-small").on("click", function () {
    onBtnChonSmall();
  })
  $("#btn-medium").on("click", function () {
    onBtnChonMedium();
  })
  $("#btn-large").on("click", function () {
    onBtnChonLarge();
  })

  $("#btn-hawai").on("click", function () {
    onBtnChonHawai();
  })
  $("#btn-hai-san").on("click", function () {
    onBtnChonHaiSan();
  })
  $("#btn-thit-hun-khoi").on("click", function () {
    onBtnChonThitHunKhoi();
  })

  $("#btn-send").on("click", function () {
    onBtnSendClick();
  })
  $("#btn-confirm").on("click", function () {
    onBtnTaoDonClick();
  })
  $("#a-orderid").on("click", function () {
    onBtnGuiDonHang();
  })
  $("#btn-cancel").on("click", function () {
    $("#btn-confirm").css("display", "block");
  })
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    loadDrink();
  }
  function onBtnGuiDonHang() {
    $("#btn-confirm").css("display", "block");
    if ($("#input-orderId").val() == "") {
      console.log("Chưa đặt đơn hàng");
    }
    else {
      var vTextAreaModal = $("#textarea-detail");
      vTextAreaModal.append("\nMã đơn hàng: " + gOrderId);
      $("#btn-confirm").css("display", "none");
      $("#detail-modal").modal("show");

    }
  }
  function onBtnChonSmall() {
    //Tạo giá trị gói small để truyền vào hàm getSize
    var vSelectedSize = getSize("S", "20cm", 2, 200, 2, 150000);
    // đổi màu button
    changeColorButton("S");
  }
  function onBtnChonMedium() {
    //Tạo giá trị gói small để truyền vào hàm getSize
    var vSelectedSize = getSize("M", "25cm", 4, 300, 3, 200000);
    // đổi màu button
    changeColorButton("M");

  }
  function onBtnChonLarge() {
    //Tạo giá trị gói small để truyền vào hàm getSize
    var vSelectedSize = getSize("L", "30cm", 8, 500, 4, 250000);
    // đổi màu button
    changeColorButton("L");

  }

  function onBtnChonHawai() {
    //Tạo giá trị gói 
    getLoaiPizza("hawai");
    // đổi màu button
    changeColorButtonType("hawai");
    console.log("Loại pizza: " + gSelectedPizzaType);
  }
  function onBtnChonHaiSan() {
    //Tạo giá trị gói 
    getLoaiPizza("haisan");
    // đổi màu button
    changeColorButtonType("haisan");
    console.log("Loại pizza: " + gSelectedPizzaType);
  }
  function onBtnChonThitHunKhoi() {
    //Tạo giá trị gói 
    getLoaiPizza("thithunkhoi");
    // đổi màu button
    changeColorButtonType("thithunkhoi");
    console.log("Loại pizza: " + gSelectedPizzaType);
  }

  function onBtnSendClick() {
    console.log("%cẤn nút kiểm tra đơn ", "color:red");

    // B1: thu thập dữ liệu
    getInfor(gInfo);
    // B2: kiểm tra dữ liệu
    var vCheck = validateUser(gInfo);

    // B3: Xử lí modal
    if (vCheck) {
      inputUser(gInfo);
      if (gInfo.magiamgia == "") { }
      else {
        if (getVoucherByVoucherId(gInfo.magiamgia) == 0)
          alert("Mã giảm giá không tồn tại");
      }
      $("#detail-modal").modal("show");
    }
  
  }



  function onBtnTaoDonClick() {
    $("#detail-modal").modal("hide");
    callApi(gInfo);
    $("#input-orderId").val(gOrderId);
    $("#thank-modal").modal("show");
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Nhập giá trị vào biến global
  //Input các tham số cho trước Output:ko
  function getSize(paramSize, paramDuongKinh, paramSuonNuong, paramSalad, paramNuocNgot, paramVND) {
    gSelectedMenuStructure.menuName = paramSize;
    gSelectedMenuStructure.duongKinhCM = paramDuongKinh;
    gSelectedMenuStructure.suonNuong = paramSuonNuong;
    gSelectedMenuStructure.saladGr = paramSalad;
    gSelectedMenuStructure.drink = paramNuocNgot;
    gSelectedMenuStructure.priceVND = paramVND;

    console.log("%cSize đã chọn: ", "color:blue");
    console.log(gSelectedMenuStructure.menuName);  //this = "đối tượng này"
    console.log("Đường kính : " + gSelectedMenuStructure.duongKinhCM);
    console.log("Sườn nướng: " + gSelectedMenuStructure.suonNuong);
    console.log("Salad: " + gSelectedMenuStructure.saladGr);
    console.log("Nước ngọt:" + gSelectedMenuStructure.drink);
    console.log("Giá: " + gSelectedMenuStructure.priceVND);
  }

  //Đổi màu nút chọn size pizza
  function changeColorButton(paramSize) {
    var vBtnSmall = $("#btn-small");  // truy vấn nút size small 
    var vBtnMedium = $("#btn-medium");
    var vBtnLarge = $("#btn-large");

    if (paramSize === "S") {  //nếu chọn Samll thì thay màu nút chọn small màu cam
      vBtnSmall.addClass("btn-warning").removeClass("bg-orange");
      vBtnMedium.addClass("bg-orange").removeClass("bg-warning");
      vBtnLarge.addClass("bg-orange").removeClass("bg-warning");
    }
    else if (paramSize === "M") {  //nếu chọn Medium thì thay màu nút medium 2 cái còn lại màu xanh
      //code đỏi màu 3 nút, khi gói pro được chọn
      vBtnSmall.addClass("bg-orange").removeClass("bg-warning");  //gán giá trị cho attribute className đẻ tác động vào class và đổi CSS
      vBtnMedium.addClass("btn-warning").removeClass("bg-orange");
      vBtnLarge.addClass("bg-orange").removeClass("bg-warning");
    }
    else if (paramSize === "L") {
      //code đỏi màu 3 nút, khi gói premium được chọn
      vBtnSmall.addClass("bg-orange").removeClass("bg-warning");
      vBtnMedium.addClass("bg-orange").removeClass("bg-warning");
      vBtnLarge.addClass("btn-warning").removeClass("bg-orange");
    }
  }

  //Nhập giá trị vào biến global với tham số truyền vào
  function getLoaiPizza(paramType) {
    if (paramType === "hawai") {
      gSelectedPizzaType = "Hawai";
    }
    else if (paramType === "haisan") {
      gSelectedPizzaType = "Hải sản";
    }
    else if (paramType === "thithunkhoi") {
      gSelectedPizzaType = "Thịt hun khói";
    }
  }
  //Đổi màu nút chọn loại pizza
  function changeColorButtonType(paramType) {
    var vBtnHawai = $("#btn-hawai");
    var vBtnHaiSan = $("#btn-hai-san");
    var vBtnThitHunKhoi = $("#btn-thit-hun-khoi");

    if (paramType === "hawai") {
      vBtnHawai.addClass("btn-warning").removeClass("bg-orange");
      vBtnHaiSan.addClass("bg-orange").removeClass("bg-warning");
      vBtnThitHunKhoi.addClass("bg-orange").removeClass("bg-warning");
    }
    else if (paramType === "haisan") {
      vBtnHawai.addClass("bg-orange").removeClass("bg-warning");
      vBtnHaiSan.addClass("btn-warning").removeClass("bg-orange");
      vBtnThitHunKhoi.addClass("bg-orange").removeClass("bg-warning");
    }
    else if (paramType === "thithunkhoi") {
      vBtnHawai.addClass("bg-orange").removeClass("bg-warning");
      vBtnHaiSan.addClass("bg-orange").removeClass("bg-warning");
      vBtnThitHunKhoi.addClass("btn-warning").removeClass("bg-orange");
    }
  }

  //Thu thập dữ liệu
  function getInfor(paramUser) {
    var vHoTen = $("#inp-fullname");
    var vEmail = $("#inp-email");
    var vDienThoai = $("#inp-dien-thoai");
    var vDiaChi = $("#inp-dia-chi");
    var vLoiNhan = $("#inp-message");
    var vMaGiamGia = $("#inp-voucher");


    paramUser.hoten = vHoTen.val().trim();
    paramUser.email = vEmail.val().trim();
    paramUser.dienthoai = vDienThoai.val().trim();
    paramUser.diachi = vDiaChi.val().trim();
    paramUser.loinhan = vLoiNhan.val().trim();
    paramUser.magiamgia = vMaGiamGia.val().trim();
  }
  function validateUser(paramUser) {
    var vSodienthoai = paramUser.dienthoai.trim();
    if (paramUser.hoten === "") {
      alert("Họ và tên không được để trống");
      return false
    }
    if (ValidateEmail(paramUser.email)) {
      alert("Email không hợp lệ");
      return false
    }
    if (isNaN(vSodienthoai)) {
      alert("Số điện thoại không hợp lệ");
      return false
    }
    if (paramUser.diachi === "") {
      alert("Địa chỉ không được để trống");
      return false
    }
    if (gSelectedMenuStructure.duongKinhCM == 0) {
      alert("Chưa chọn combo");
      return false
    }
    if (gSelectedPizzaType == "") {
      alert("Chưa chọn loại pizza");
      return false
    }
    if ($('#select-drink').val() == 0) {
      alert("Chưa chọn đồ uống");
      return false
    }
    return true;
  }
  function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return false
    }
    return true
  }

  function inputUser(paramUser) {
    $("#input-name").val(paramUser.hoten);
    $("#input-phone").val(paramUser.dienthoai);
    $("#input-dia-chi").val(paramUser.diachi);
    $("#input-loi-nhan").val(paramUser.loinhan);
    $("#input-magiamgia").val(paramUser.magiamgia);

    var vTextAreaModal = $("#textarea-detail");
    var vLoaiNuocUong = $('#select-drink :selected').text();
    var vGiamGia = getVoucherByVoucherId(paramUser.magiamgia);

    vTextAreaModal.html("Xác nhận:" + paramUser.hoten + ", " + paramUser.dienthoai + ", " + paramUser.diachi)
    vTextAreaModal.append("\nSize: " + gSelectedMenuStructure.menuName);
    vTextAreaModal.append("\nĐường kính : " + gSelectedMenuStructure.duongKinhCM);
    vTextAreaModal.append("\nSườn nướng: " + gSelectedMenuStructure.suonNuong);
    vTextAreaModal.append("\nSalad: " + gSelectedMenuStructure.saladGr);
    vTextAreaModal.append("\nNước ngọt: " + gSelectedMenuStructure.drink);
    vTextAreaModal.append("\nGiá: " + gSelectedMenuStructure.priceVND);
    vTextAreaModal.append("\nPhải thanh toán: " + ((gSelectedMenuStructure.priceVND * (100 - vGiamGia)) / 100) + " (giảm giá: " + vGiamGia + "%)");
    vTextAreaModal.append("\nLoại nước uống: " + vLoaiNuocUong);
    vTextAreaModal.append("\nLoại pizza: " + gSelectedPizzaType);
  }


  //Tìm mã giảm giá ứng với input
  function getVoucherByVoucherId(paramVoucher) {
    var vGiamGia = 0;
    $.ajax({
      url: "http://localhost:8000/devcamp-pizza365/vouchers/" + paramVoucher,
      type: 'GET',
      dataType: 'json', // added data type
      async: false,
      success: function (responseObject) {
        console.log(responseObject.data)
        vGiamGia = responseObject.data.phanTramGiamGia;
      },
      error: function (ajaxContext) {
        console.log(ajaxContext)
      }
    });
    console.log(vGiamGia);
    return vGiamGia;
  }

  function loadDrink() {
    $.ajax({
      url: "http://localhost:8000/devcamp-pizza365/drinks",
      type: 'GET',
      dataType: 'json', // added data type
      async: false,
      success: function (responseObject) {
        allDrinkToSelect(responseObject.drinks);
      },
      error: function (ajaxContext) {
        alert(ajaxContext)
      }
    });
  }

  function allDrinkToSelect(paramDrinkList) {
    for (var bI = 0; bI < paramDrinkList.length; bI++) {
      $('#select-drink').append($('<option>', {
        value: paramDrinkList[bI].maNuocUong,
        text: paramDrinkList[bI].tenNuocUong
      }));
    }
  }

  function callApi(paramUser) {

    var vObjectRequest = {
      kichCo: "",
      duongKinh: "",
      suon: "",

      salad: "",
      loaiPizza: "",
      idVourcher: -1,

      idLoaiNuocUong: "",
      soLuongNuoc: -1,
      hoTen: "",

      thanhTien: "",
      email: "",
      soDienThoai: "",

      diaChi: "",
      loiNhan: ""
    }

    vObjectRequest.kichCo = gSelectedMenuStructure.menuName;
    vObjectRequest.duongKinh = gSelectedMenuStructure.duongKinhCM;
    vObjectRequest.suon = gSelectedMenuStructure.suonNuong;

    vObjectRequest.salad = gSelectedMenuStructure.saladGr;
    vObjectRequest.loaiPizza = gSelectedPizzaType;
    vObjectRequest.idVourcher = paramUser.magiamgia;

    vObjectRequest.idLoaiNuocUong = $("#select-drink").prop("selectedIndex");
    vObjectRequest.soLuongNuoc = gSelectedMenuStructure.drink;
    vObjectRequest.hoTen = paramUser.hoten;

    vObjectRequest.thanhTien = gSelectedMenuStructure.priceVND;
    vObjectRequest.email = paramUser.email;
    vObjectRequest.soDienThoai = paramUser.dienthoai;

    vObjectRequest.diaChi = paramUser.diachi;
    vObjectRequest.loiNhan = paramUser.loinhan;
    console.log(vObjectRequest);
    $.ajax({
      url: "http://localhost:8000/devcamp-pizza365/orders",
      type: 'POST',
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjectRequest),
      async: false,
      success: function (responseObject) {
        console.log(responseObject);
        gOrderId = responseObject.data.orderId;
      },
      error: function (ajaxContext) {
        alert("thất bại")
      }
    });
  }
})